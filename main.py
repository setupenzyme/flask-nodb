from flask import Flask

from server.routes import router

app = Flask(__name__, static_folder='web/static', template_folder='web/templates')
app.register_blueprint(router)

@app.get('/health')
def health():
  return {'health': "good"}

if __name__ == "__main__":
    app.run(debug=True)