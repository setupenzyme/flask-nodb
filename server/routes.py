from flask import Blueprint, render_template

router = Blueprint('routes', __name__, static_folder='../web/static', template_folder='../web/templates')

@router.route('/', methods=['POST', 'GET'])
def index():
    return render_template('index.html')